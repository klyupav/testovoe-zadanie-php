Тестовое задание для PHP-программиста
=====================================

JUNIOR-уровень
--------------

Измените существующий код так, чтобы на главной странице приложения было:

- ~~текущее время и дата;~~
- ~~название класса контроллера, который отвечает за отображение этой страницы;~~
- ~~название метода контроллера, который отправляет данные на эту страницу;~~

MIDDLE-уровень
--------------

Дополните существующий код так, чтобы:

- ~~в БД приложения были импортированы (с помощью консольной команды) **10 последних записей** (не больше) из [iTunes Movie Trailers](https://trailers.apple.com);~~
- ~~на главной странице приложения были показаны эти 10 записей. Каждая должна состоять из заголовка трейлера и постера;~~
- ~~заголовок каждой записи должен быть ссылкой на подробную страницу трейлера. На подробной странице кроме заголовка и постера должно быть описание трейлера и ссылка на источиник, а так же ссылка для возврата к странице списка;~~
- ~~имейте в виду, что приложение будет разворачиваться и тестироваться на независимом хосте — у вас должно быть описано, как запустить обновление БД и импорт данных;~~
- ~~будьте готовы объяснить ваше решение.~~

Дополнительные скилы
--------------------

- ~~Выполните все этапы для middle;~~
- ~~предложите другую схему получения rss и преобразования полученного в итоговый класс;~~
- предожите схему валидации полей сущностей;
- добавьте пользователей;
- реализуйте отметки «Нравится» для загруженных трейлеров, покажите количество таких отметок для каждого из трейлеров;
- ~~напишите тесты;~~

------------------------

Инструкция по запуску проекта
-----------------------------

- По возможности используйте docker для запуска приложения. Docker-compose.yaml готов к использованию;
- Чтобы инициализировать таблицы БД, используйте встроенную консольную команду `orm:schema-tool:update --force`
- Чтобы импортировать в таблицу БД 10 последних записей, используйте встроенную консольную команду `fetch:trailers`;
- Чтобы запустить веб сервер, в контейнере с приложением, перейдите в папку `public` и выполните команду `php -S 0.0.0.0:8080`;
