<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class MovieRepository extends EntityRepository
{
    public function updateOrCreate(array $attributes): Movie
    {
        $title = $attributes['title'] ?? null;
        $description = $attributes['description'] ?? null;
        $link = $attributes['link'] ?? null;
        $pub_date = $attributes['pub_date'] ?? null;
        $image = $attributes['image'] ?? null;

        $item = $this->_em->getRepository(Movie::class)->findOneBy(compact('title'));

        if ($item === null) {
             $item = new Movie();
        }

        if ($title) {
            $item->setTitle($title);
        }
        if ($link) {
            $item->setLink($link);
        }
        if ($description) {
            $item->setDescription($description);
        }
        if ($pub_date) {
            $item->setPubDate($this->parseDate($pub_date));
        }
        if ($image) {
            $item->setImage($image);
        }

        return $item;
    }

    public function truncateData(): void
    {
        $tableName = $this->_em->getClassMetadata(Movie::class)->getTableName();
        $connection = $this->_em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $truncateSql = $platform->getTruncateTableSQL($tableName);
        $connection->executeStatement($truncateSql);
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }

    public static function fetchData(EntityManagerInterface $entityManager): Collection
    {
        $data = $entityManager->getRepository(Movie::class)->findAll();

        return new ArrayCollection($data);
    }

    protected function parseDate(string $date): \DateTime
    {
        return new \DateTime($date);
    }
}
