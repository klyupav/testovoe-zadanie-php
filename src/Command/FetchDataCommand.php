<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FetchDataCommand extends Command
{
    private const SOURCE = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss';

    protected static $defaultName = 'fetch:trailers';

    public const LIMIT = 10;

    private ClientInterface $httpClient;
    private LoggerInterface $logger;
    private string $source;
    private EntityManagerInterface $doctrine;
    private MovieRepository $movieRepository;

    /**
     * FetchDataCommand constructor.
     *
     * @param ClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     * @param string|null $name
     */
    public function __construct(ClientInterface $httpClient, LoggerInterface $logger, EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->doctrine = $em;
        $this->movieRepository = new MovieRepository($em, new ClassMetadata(Movie::class));
    }

    public function configure(): void
    {
        $this
            ->setDescription('Fetch data from iTunes Movie Trailers')
            ->addArgument('source', InputArgument::OPTIONAL, 'Overwrite source');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info(sprintf('Start %s at %s', __CLASS__, (string)date_create()->format(DATE_ATOM)));
        $source = self::SOURCE;
        if ($input->getArgument('source')) {
            $source = $input->getArgument('source');
        }

        if (!is_string($source)) {
            throw new RuntimeException('Source must be string');
        }
        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Fetch data from %s', $source));

        try {
            $response = $this->httpClient->sendRequest(new Request('GET', $source));
        } catch (ClientExceptionInterface $e) {
            throw new RuntimeException($e->getMessage());
        }
        if (($status = $response->getStatusCode()) !== 200) {
            throw new RuntimeException(sprintf('Response status is %d, expected %d', $status, 200));
        }
        $data = $response->getBody()->getContents();
        $this->processXml($data);

        $this->logger->info(sprintf('End %s at %s', __CLASS__, (string)date_create()->format(DATE_ATOM)));

        return 0;
    }

    protected function processXml(string $data): void
    {
        $xml = (new \SimpleXMLElement($data))->children();

        if (!property_exists($xml, 'channel')) {
            throw new RuntimeException('Could not find \'channel\' element in feed');
        }

        $this->movieRepository->truncateData();

        $i = 0;
        foreach ($xml->channel->item as $item) {
            /**
             * @var \SimpleXMLElement $item
             * @var \SimpleXMLElement $encoded
             */
            $encoded = $item->children("content", true)->encoded;

            $attrMovie = [
                'title' => (string)$item->title,
                'description' => (string)$item->description,
                'link' => (string)$item->link,
                'pub_date' => (string)$item->pubDate,
                'image' => $this->parsePoster((string)$encoded),
            ];

            $movie = $this->movieRepository->updateOrCreate($attrMovie);

            if ($movie->getId() === null) {
                $this->logger->info('Create new Movie', ['title' => $attrMovie['title']]);
            } else {
                $this->logger->info('Move found', ['title' => $attrMovie['title']]);
            }

            $this->doctrine->persist($movie);
            $i++;

            if ($i === self::LIMIT) {
                break;
            }
        }

        $this->doctrine->flush();
    }

    protected function parsePoster(string $content): ?string
    {
        /**
         * @var \DOMElement[] $img
         */
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);

        $img = $dom->getElementsByTagName('img');

        if (!empty($img)) {
            return $img[0]->getAttribute('src');
        }

        return null;
    }
}
