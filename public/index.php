<?php

/** @var ContainerInterface $container */
use Psr\Container\ContainerInterface;
use Slim\Middleware\ErrorMiddleware;
use Slim\Psr7\Factory\ServerRequestFactory;

$container = require dirname(__DIR__) . '/bootstrap.php';

$app = Slim\Factory\AppFactory::createFromContainer($container);
$app->add($container->get(ErrorMiddleware::class));

$request = ServerRequestFactory::createFromGlobals();

$app->run($request);
