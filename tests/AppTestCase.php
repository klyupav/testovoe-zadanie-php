<?php


namespace Tests;


use App\Command\FetchDataCommand;
use App\Container\Container;
use App\Support\CommandMap;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteInterface;
use Slim\Middleware\ErrorMiddleware;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\UriFactory;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class AppTestCase extends TestCase
{
    private Application $console;
    private App $app;
    private EntityManager $em;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->app = self::createApplication();
        $this->console = $this->createConsoleApplication();
        $this->em = $this->app->getContainer()->get(EntityManagerInterface::class);
        parent::__construct($name, $data, $dataName);
    }

    public function getRouteByPattern(string $pattern): ?RouteInterface
    {
        /**
         * @var RouteCollectorInterface $router
         */
        $router = $this->app->getContainer()->get(RouteCollectorInterface::class);
        foreach ($router->getRoutes() as $route) {
            if ($route->getPattern() === $pattern) {
                return $route;
            }
        }

        return null;
    }

    public function request(string $method, $uri): ResponseInterface
    {
        $uri = (new UriFactory())->createUri($uri);
        $server = new ServerRequestFactory();

        $request = $server->createServerRequest(strtoupper($method), $uri);

        $response = $this->app->handle($request);
        $response->getBody()->rewind();

        return $response;
    }

    public function getEntityManager(): EntityManager
    {
        return $this->em;
    }

    public function getConsole(): Application
    {
        return $this->console;
    }

    public function getApp(): App
    {
        return $this->app;
    }

    public function resetDatabase(): void
    {
        self::runCommand($this->console, 'orm:schema-tool:drop', [
            '--force' => true,
        ]);

        self::runCommand($this->console, 'orm:schema-tool:update', [
            '--force' => true,
        ]);
    }

    public function createConsoleApplication(): Application
    {
        $container = self::bootLoader();

        $response = new Response(200, [], file_get_contents(__DIR__ . '/_tests_data/newtrailers.rss'));

        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->expects($this->any())
            ->method('sendRequest')
            ->willReturn($response);

        $logger = $this->createMock(LoggerInterface::class);

        $container->set(FetchDataCommand::class, static function (ContainerInterface $container) use ($httpClient, $logger)  {
            return new FetchDataCommand(
                $httpClient,
                $logger,
                $container->get(EntityManagerInterface::class)
            );
        });

        $loader = new ContainerCommandLoader($container, $container->get(CommandMap::class)->getMap());

        $app = new Application();

        $helperSet = ConsoleRunner::createHelperSet($container->get(EntityManager::class));
        $app->setHelperSet($helperSet);
        $app->setCatchExceptions(true);
        $app->setAutoExit(false);
        ConsoleRunner::addCommands($app);

        $app->setCommandLoader($loader);

        return $app;
    }

    public static function createApplication(): App
    {
        $container = self::bootLoader();
        $app = AppFactory::createFromContainer($container);
        $app->add($container->get(ErrorMiddleware::class));

        return $app;
    }

    public static function runCommand(Application $application, string $command, array $parameters = []): void
    {
        $exit = $application->run(
            new ArrayInput(\array_merge(['command' => $command], $parameters)),
            $output = new BufferedOutput()
        );

        if (0 !== $exit) {
            throw new \RuntimeException(\sprintf('Error running "%s": %s', $command, $output->fetch()));
        }
    }

    private static function bootLoader(): Container
    {
        $container = require __DIR__ . '/bootstrap.php';
        return $container;
    }
}
