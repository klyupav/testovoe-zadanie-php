<?php


namespace Tests\Command;

use App\Command\FetchDataCommand;
use App\Entity\Movie;
use App\Repository\MovieRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Tests\AppTestCase;

class FetchDataCommandTest extends AppTestCase
{
    public function setUp(): void
    {
        $this->resetDatabase();
    }

    public function testImportTrailers()
    {
        $movieRepository = new MovieRepository($this->getEntityManager(), new ClassMetadata(Movie::class));

        $this->assertCount(0, $movieRepository->findAll());

        self::runCommand($this->getConsole(), 'fetch:trailers');

        $count = count($movieRepository->findAll());

        $this->assertGreaterThan(0, $count);
        $this->assertLessThanOrEqual(FetchDataCommand::LIMIT, $count);
    }
}
