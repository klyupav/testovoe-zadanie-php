<?php


namespace Tests\Controller;

use App\Entity\Movie;
use Symfony\Component\DomCrawler\Crawler;
use Tests\AppTestCase;

class HomeControllerTest extends AppTestCase
{
    public function setUp(): void
    {
        $this->resetDatabase();
    }

    public function testMoviesAreShownOnTheHomepage()
    {
        $em = $this->getEntityManager();

        $movie1 = (new Movie())
            ->setTitle('Movie1')
            ->setDescription('Description')
            ->setLink('#')
            ->setPubDate(new \DateTime());
        $em->persist($movie1);
        $movie2 = (new Movie())
            ->setTitle('Movie2')
            ->setDescription('Description')
            ->setLink('#')
            ->setPubDate(new \DateTime());
        $em->persist($movie2);
        $em->flush();

        $this->assertCount(2, $em->getRepository(Movie::class)->findAll());

        $response = $this->request('GET', '/');
        $this->assertSame(200, $response->getStatusCode());
        $crawler = new Crawler($response->getBody()->getContents());

        $linkToDetailPage = $crawler->filter('.container .item a');

        $this->assertSame('/trailers/' . $movie2->getId() , $linkToDetailPage->last()->attr('href'));
        $this->assertSame((new \DateTime())->format('Y-m-d H:i') , $crawler->filter('.container .time')->text());
        $this->assertSame($this->getRouteByPattern('/')->getCallable() , $crawler->filter('.container .action')->text());
        $this->assertCount(1 , $crawler->filter('.container h1'));
        $this->assertCount(2, $crawler->filter('.container .poster'));
    }

    public function testMovieAreShownOnTheDetailPage()
    {
        $em = $this->getEntityManager();

        $movie = (new Movie())
            ->setTitle('Movie1')
            ->setDescription('Description')
            ->setLink('#')
            ->setPubDate(new \DateTime());
        $em->persist($movie);
        $em->flush();

        $findMovie = $em->getRepository(Movie::class)->find($movie->getId());
        $this->assertTrue(!empty($findMovie));

        $response = $this->request('GET', '/trailers/' . $movie->getId());
        $this->assertSame(200, $response->getStatusCode());
        $crawler = new Crawler($response->getBody()->getContents());

        $this->assertSame($movie->getTitle() , $crawler->filter('.container h1')->text());
        $this->assertCount(1 , $crawler->filter('.container .description'));
        $this->assertCount(1, $crawler->filter('.container .poster'));
        $this->assertCount(1, $crawler->filter('.container .source'));
        $this->assertCount(1, $crawler->filter('.container .back'));
    }
}
